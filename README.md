# Serverless Redirect URL Example

This is an example to use serverless AWS Lambda to make a redirect URL service.

Use case: You have kibana URL with long URL name and want to make a human readble shortcut URL.

From: 'https://96d2ca45a01a423e84ace8a4fcf5df0a.us-west-1.aws.found.io:9243'
to: https://kibana.mydomain.com

Install serverless from nodejs npm
```
npm install -g serverless
```

## Create our serverless service
```
serverless create --template aws-nodejs --path kibana
```

## Add your function in handler.js and update serverless.yml
handler.js

> Change Location URL in handler.js. I keep the real long kibana URL in stageVariables

```
'use strict';

module.exports.kibana_redirect = async event => {
  let url = "https://www.google.com";
  if (event.stageVariables && event.stageVariables["TO_URL"]) {
    url = event.stageVariables["TO_URL"]
  }

  return {
    statusCode: 301,
     headers: {
            Location: url
     }
  };
}
```

serverless.yml
```
service: kibana
# app and org for use with dashboard.serverless.com
app: kibana

frameworkVersion: '2'

provider:
  name: aws
  runtime: nodejs12.x

  stage: prod
  region: us-west-1

functions:
  kibana_redirect:
    handler: handler.kibana_redirect
    events:
     - http:
         path: /
         method: any
```

## Deploy to AWS
Before you can deploy, you need to configure AWS Credentials with persmission AWSCodeDeployRoleForLambdaLimited. Create access key in Security Credentials tab, then enter your access key id and secret using:
```
aws configure
```

This will deploy to default stage in serverless.yml
```
serverless deploy
```

If you want to deploy to other stage and region
```
serverless deploy --stage dev --region us-west-1
```

## Create custom domain names
Next, create custom domain names and configure the mapping to your API. Then create a domain/subdomain in route53 with alias to your API Gateway domain name

Example:
```
API Gateway domain name
dlwap1zsmjxxx.cloudfront.net
```

In Route53 create record set
```
kibana.mydomain.com. A ALIAS dlwap1zsmjxxx.cloudfront.net
```