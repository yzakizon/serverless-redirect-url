'use strict';

module.exports.kibana_redirect = async event => {
  let url = "https://www.google.com";
  if (event.stageVariables && event.stageVariables["TO_URL"]) {
    url = event.stageVariables["TO_URL"]
  }

  return {
    statusCode: 301,
     headers: {
            Location: url
     }
  };
}